/**
* Sample React Native App
* https://github.com/facebook/react-native
* @flow
*/

import React, { Component } from 'react';
import {
Platform,
StyleSheet,
Text,
View,
Switch,
Image,
PixelRatio,
KeyboardAvoidingView,
ScrollView,
TouchableHighlight,
TouchableOpacity,
AsyncStorage,
ActivityIndicator
} from 'react-native';

import Carousel, { Pagination,ParallaxImage } from 'react-native-snap-carousel';

import { NavigationActions } from 'react-navigation';

import FontAwesome from 'react-native-vector-icons/FontAwesome';
import  Entypo from 'react-native-vector-icons/Entypo'
import EvilIcons from 'react-native-vector-icons/EvilIcons'
import Feather from 'react-native-vector-icons/Feather'

import { LineChart, YAxis } from 'react-native-svg-charts'
import { LinearGradient, Stop } from 'react-native-svg'
import { onSignIn, isSignedIn, onSignOut } from '../../../config/auth';
import { getCourses } from '../../template/Courses/courses.js'
import * as shape from 'd3-shape'
const Dimensions = require('Dimensions');
const {height, width} = Dimensions.get('window');


import SplashScreen from 'react-native-smart-splash-screen'

import styles from '../Tabs/style.js'
const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

function wp (percentage) {
  const value = (percentage * viewportWidth) / 100;
  return Math.round(value);
}

import { ENTRIES2 } from '../Tabs/entries';

const slideHeight = viewportHeight * 0.4;
const slideWidth = wp(90);
const itemHorizontalMargin = wp(2);
const sliderWidth = viewportWidth;
const itemWidth = slideWidth + itemHorizontalMargin * 2;


export default class Courses extends Component<{}> {
  constructor(props) {
      super(props);
      this.state={
        courses:[],
        user:{id:'', name:''},
        slider1Ref :null,
        slider1ActiveSlide: 1,
        loader:null
      }
  }
  componentWillMount(){
    this.setState({loader:true})
    this.getStudentID();

  }
  async getStudentID () {
  console.log("ff")
  var state = this.state;
    //let USER_ID = await AsyncStorage.getItem("USER_ID");
    let USER_ID = await AsyncStorage.getItem("USER_ID");
    let USER_NAME = await AsyncStorage.getItem("USER_NAME");
    state.user.id=USER_ID;
    state.user.name = USER_NAME;
    this.setState(state,()=>{
      this._getAllCourse(this.state.user.id);
    });
  }
  _getAllCourse=(id)=>{
    this.setState({loader:true})
    getCourses(id).then(response=>{
       this.setState({loader:false})
      console.log(response.status, response)
      if(response.status){
          this.setState({courses:response.data}, ()=>console.log(this.state));
      }else{
        ToastAndroid.show(response.show, ToastAndroid.SHORT);
      }
    },err=>{
      this.setState({loader:false})
      console.log(err)

    })
  }

  previousPage() {
    const { navigate  } = this.props.navigation;
    navigate("UpcomingWorkouts");
  }


    _renderWorkout= ({item, index},parallaxProps)=> {


    return (
        <View >

          <TouchableOpacity
              activeOpacity={1}
              style={styles.carousel_style_workout}
              onPress={() => { this.routeSubcribe(item); }}
              >

                <View style={styles.carousel_image_view}>

                    <ParallaxImage
                    source={{ uri: 'http://i.imgur.com/UPrs1EWl.jpg' }}
                    containerStyle={{flex: 1,backgroundColor: 'white',}}
                    style={styles.carousel_image}
                    parallaxFactor={0.35}
                    showSpinner={true}
                    spinnerColor={'rgba(0, 0, 0, 0.25)'}
                    {...parallaxProps}
                    />

                </View>
                <View style={styles.carousel_text_view}>
                  <Text
                    style={styles.carousel_text}
                    numberOfLines={2}
                  >
                  {item.title}
                  </Text>

                  </View>
            </TouchableOpacity>
        </View>
    );
}


    _renderTrainers= ({item, index},parallaxProps)=> {

      return (
          <View >
            <TouchableOpacity
                activeOpacity={1}
                style={styles.carousel_style_trainers}
                onPress={() => { alert('hi'); }}
                >
                  <View style={styles.carousel_image_view}>

                      <ParallaxImage
                      source={{ uri: item.illustration }}
                      containerStyle={{flex: 1,backgroundColor: 'white',}}
                      style={styles.carousel_image}
                      parallaxFactor={0.35}
                      showSpinner={true}
                      spinnerColor={'rgba(0, 0, 0, 0.25)'}
                      {...parallaxProps}
              />

                  </View>
                  <View style={styles.carousel_text_view}>
                      <Text
                        style={styles.carousel_text}
                        numberOfLines={2}
                      >
                      some text
                      </Text>

                  </View>
              </TouchableOpacity>
          </View>
      );
  }


      _renderPlans= ({item, index},parallaxProps)=> {

        return (
            <View >
              <TouchableOpacity
                  activeOpacity={1}
                  style={styles.carousel_style_plans}
                  onPress={() => { alert('hi'); }}
                  >
                    <View style={styles.carousel_image_view}>

                        <ParallaxImage
                        source={{ uri: item.illustration }}
                        containerStyle={{flex: 1,backgroundColor: 'white',}}
                        style={styles.carousel_image}
                        parallaxFactor={0.35}
                        showSpinner={true}
                        spinnerColor={'rgba(0, 0, 0, 0.25)'}
                        {...parallaxProps}
                />

                    </View>
                    <View style={styles.carousel_text_view}>
                        <Text
                          style={styles.carousel_text}
                          numberOfLines={2}
                        >
                          some text
                        </Text>

                    </View>
                </TouchableOpacity>
            </View>
        );
    }

routeSubcribe=(course)=>{
    console.log(course)
    const { navigate  } = this.props.navigation;
    navigate('Subscribe',{course:course})
  }

render() {
  const {navigation} = this.props;

  const resizeMode = 'center';
  const text = 'I am some centered text';
  return (

    <View style={styles.mainBody} >


          <ScrollView
            style={{flex:1}}
            contentContainerStyle={{paddingBottom: 50}}
            indicatorStyle={'white'}
            scrollEventThrottle={200}
            directionalLockEnabled={true}
          >
            <View style={styles.listofWrkouts1}>
              <View style={styles.chevron_left_icon}>
                <TouchableOpacity onPress = {()=>this.previousPage()}>
                    <FontAwesome name="chevron-left" size={25} color="#FF7E00"   />
                </TouchableOpacity>
              </View>

              <View style={styles.header}>
                    <Text style={styles.topSignupTxt}>
                      ALL COURSES
                    </Text>
              </View>

              <View>
                <Text style = {styles.text_workout_heading}>
                  Hey {this.state.user.name}!
                </Text>
                <Text style = {styles.text_workout_sub_heading}>
                  REMAINDER TO STAY STRONG AND KEEP YOUR GOALS IN MIND
                </Text>
              </View>
              {this.state.loader?
                <ActivityIndicator
                  animating = {this.state.loader}
                  color = '#bc2b78'
                  size = "large"
                  style = {styles.activityIndicator}
                />
                :
                this.state.courses.length?
                <View style={styles.slidertop_btns}>
                  <View style={{flex:1,flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
                    <View style = {{flex:0.28,alignItems:'center',justifyContent:'center',}}>
                    <TouchableOpacity>
                      <View style={styles.gradi_btns}>
                        <Text style={styles.gradi_NormalbtnsTxtBtn}>
                            Distance
                        </Text>
                        <View style={styles.gradiPagBtns}></View>
                      </View>
                    </TouchableOpacity>
                    </View>
                    <View style = {{flex:0.28,alignItems:'center',justifyContent:'center',}}>
                    <TouchableOpacity>
                    <View style={styles.gradi_btns}>
                      <Text style={styles.gradi_NormalbtnsTxtBtn}>
                          Weights
                      </Text>
                      <View style={styles.gradiPagBtns}></View>
                    </View>
                    </TouchableOpacity>
                    </View>
                    <View style = {{flex:0.28,alignItems:'center',justifyContent:'center',}}>
                    <TouchableOpacity>
                    <View style={styles.gradi_btns}>
                      <Text style={styles.gradi_btnsTxtBtn}>
                          Conditioning
                      </Text>
                      <Image style={{ width: 86, height: 32 }} source={{uri:'gradi_btn'}} />
                      </View>
                    </TouchableOpacity>
                    </View>
                    <View style = {{flex:0.16,alignItems:'center',justifyContent:'center',}}>
                      <TouchableOpacity >
                          <View>
                            <Feather name="sliders" size={25} color='#fff' />
                          </View>
                      </TouchableOpacity>

                    </View>

                  </View>
                </View>
                :
                null
              }
            </View>


            <View>

                  {this.state.loader?
                  null
                  :
                    this.state.courses.length?
                  <Carousel
                    ref={(c) => { if (!this.state.slider1Ref) { this.setState({ slider1Ref: c }); } }}
                    data={this.state.courses}
                    renderItem={this._renderWorkout}
                    sliderWidth={sliderWidth}
                    itemWidth={200}
                    itemHeight={300}
                    hasParallaxImages={true}
                    firstItem={1}

                    containerCustomStyle={{marginTop: 15}}

                     itemWidth={itemWidth}
                    enableSnap = {true}
                    lockScrollWhileSnapping = {true}
                    onSnapToItem={(index) => this.setState({ slider1ActiveSlide: index }) }
                  />
                  :<Text style={styles.upcomingWorkouts_heading}>
                      YOU HAVE ASSIGNED TO ALL COURSES.. WAIT FOR NEW COURSE TO COME
                    </Text>}
              </View>


          </ScrollView>


          </View>

  );
}
}
