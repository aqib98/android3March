/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Switch,
  Image,
  PixelRatio,
  KeyboardAvoidingView,
  ScrollView,
  TouchableHighlight,
  TouchableOpacity,
  TextInput

} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';
import { NavigationActions } from 'react-navigation';
import { LineChart, YAxis } from 'react-native-svg-charts'
import { LinearGradient, Stop } from 'react-native-svg'

import * as shape from 'd3-shape'
const Dimensions = require('Dimensions');
const {height, width} = Dimensions.get('window');

import SplashScreen from 'react-native-smart-splash-screen'
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import  Entypo from 'react-native-vector-icons/Entypo'
import EvilIcons from 'react-native-vector-icons/EvilIcons'
import Feather from 'react-native-vector-icons/Feather'
import Ionicons from 'react-native-vector-icons/Ionicons'
import { AsyncStorage } from "react-native";
import { getWorkoutDetails } from '../template/Workout/workout.js';
import { getExercises  } from '../template/api.js';

import styles from './styles.js'

export default class WorkoutDetails extends Component {

  constructor(props) {
    super(props);
    this.state={
      workoutdata: null,
      user_id:''
    }
  }


  async componentWillMount(){
    console.log(this.props.navigation.state.params)
    var { params } = this.props.navigation.state.params;
    let USER_ID = await AsyncStorage.getItem("USER_ID");
    this.setState({user_id:USER_ID})
    this.getWorkoutDetails(params.id)
    this._getAllExercises(params.id, USER_ID, params.workout_day);
    console.log(this.props.navigation.state.params)
  }

  getWorkoutDetails(workoutID) {

    getWorkoutDetails(workoutID).then(response=>{
      console.log(response.data.workout)
      this.setState({workoutdata:response.data.workout.id});
      this.setState({ImageUrl:response.data.workout.thumbnail_url});
      this.setState({Title:response.data.workout.title});
      this.setState({Duration:response.data.workout.duration});
      this.setState({intensity:response.data.workout.intensity});
      console.log(response.data.workout.back_thumbnail_url)
    },err=>{
      console.log(err)
    })

  }

  _getAllExercises=(workoutID, user_id, workout_day)=>{
    this.setState({loader:true})
    getExercises(workoutID, user_id, workout_day).then(response=>{
      console.log(response)
      this.setState({exercises:response.data},()=>{

        console.log(this.state.exercises[0].id)

      });
    },err=>{
      console.log(err)
    })
  }

  nextpage() {
    const { navigate  } = this.props.navigation;
    var { params } = this.props.navigation.state.params;
    var parameter = {
      exerciseID: this.state.exercises[0].id,
      workoutID: params.id,
      courseID: params.course_id,
      workout_day: params.workout_day,
      user_id: this.state.user_id,
      workout_title:params.title,
      streaming_uri:this.state.exercises[0].streaming_uri+'(format=m3u8-aapl)'
    }
    navigate('Videoactivity', { params:parameter });
  }

  render() {
    const {navigation} = this.props
    const resizeMode = 'center';
    const text = 'I am some centered text';
    return (

      <View style={styles.mainBody} >


            <ScrollView
              style={{flex:1}}
              contentContainerStyle={{paddingBottom: 50}}
              indicatorStyle={'white'}
              scrollEventThrottle={200}
              directionalLockEnabled={true}
            >
              <View style={styles.listofWrkouts21}>
                <View style={styles.chevron_left_icon}>
                  <TouchableOpacity onPress={()=>this.onPressChevronLeft(navigation )}>
                      <FontAwesome name="chevron-left" size={25} color="#FF7E00"   />
                  </TouchableOpacity>
                </View>
                <View style={styles.header_wrkout_details}>
                       <Image style={{width: Dimensions.get('window').width, height:260 }} source={{uri:this.state.ImageUrl}} />

                </View>
                <View>
                    <Text style={styles.wrkdetails_name1}>
                      {this.state.Title}
                    </Text>
                    <View style={styles.wrkdetails_name1abc_mas}>
                      <Text style={styles.wrkdetails_name1a}> {this.state.Duration} MIN!</Text>
                      {this.state.intensity<5?<Text style={styles.wrkdetails_name1b}> LOW LEVEL</Text>
                      :<Text style={styles.wrkdetails_name1b}> HARD LEVEL</Text>}
                      <Text style={styles.wrkdetails_name1c}>{this.state.tags}</Text>
                    </View>
                </View>

              </View>





            </ScrollView>




            <View style={styles.footer}>

                <TouchableOpacity onPress = {()=>this.nextpage()}>
                  <View style={styles.save_view}>
                      <Text style={styles.save_btnTxt}>Start Workout</Text>
                  </View>

                  <Image
                    style={styles.save_btnImg}
                    source={{ uri: 'buttonimg' }}
                  />

                </TouchableOpacity>
              </View>



            </View>

    );
  }
}
