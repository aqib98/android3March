
'use strict';

import React, { Component } from 'react';
import { View, StyleSheet, TouchableHighlight, Text, Image, ActivityIndicator } from 'react-native';
import { Card, Button, FormLabel, FormInput, FormValidationMessage } from "react-native-elements";
import { getSensorData } from '../template/api.js';

import Icon from 'react-native-vector-icons/FontAwesome';
import { NavigationActions } from 'react-navigation';
import { LineChart, YAxis, XAxis } from 'react-native-svg-charts'
import { LinearGradient, Stop } from 'react-native-svg'

import * as shape from 'd3-shape'
const Dimensions = require('Dimensions');
const {height, width} = Dimensions.get('window');
import styles from './styles.js'


class TrackActivitySummary extends Component {

  constructor(props) {
    super(props);
    this.state={
     avg_heart_rate:[],
    }
}

  componentWillMount(){

    this.setState({loader:false});
    console.log(this.props.navigation.state.params.avg_heart_rate);
    this.state.avg_heart_rate = this.props.navigation.state.params.avg_heart_rate

  }


  nextpage(){

    const { navigate  } = this.props.navigation;

    console.log("Navigation")

    navigate("FinishExcierce")

  }

  render() {
    const {navigation} = this.props;
    const {  loader  } = this.state;

    var { calories, time, workout_title  } = this.props.navigation.state.params;

    const data = this.state.avg_heart_rate

    //const data = [ 0, 90, 110, 120, 150, 180, 170, 170, 165, 189, 150, 100, 90, 95, 0 ]

    const cut = 0.5
    const cutBuffer = 0.001

    const colors = [ 'rgb(138, 0, 230, 0.8)', 'rgb(173, 51, 255, 0.8)', 'rgb(194, 102, 255, 0.8)', 'rgb(214, 153, 255, 0.8)' ]

    return (
      <View style={styles.upcomingWorkouts_main_body}>
              <View style={styles.upcomingWorkouts_bg_img_view}>
                <Image style={styles.upcomingWorkouts_bg_img} source={{ uri: 'gradientbg' }}/>
              </View>
              <View style={styles.upcomingWorkouts_header}>
              <Text style={styles.graph_heading}>
                  WORKOUT SUMMARY!
              </Text>
              </View>

              {loader?
                <ActivityIndicator
                  animating = {this.state.loader}
                  color = '#bc2b78'
                  size = "large"
                />
                :
                <View style={styles.graph_bg}>

              <Text style={styles.basketBall}>{workout_title}</Text>
              <Text style={styles.basketDay}>{time} MIN {calories} CALORIES</Text>
              <Image style={{  width: 45, height: 45 }} source={{uri:'resoltz_logo_only'}} />
               <YAxis
                  style={ { paddingHorizontal: 16 } }
                    dataPoints={ data }
                    contentInset={ { top: 40, bottom: 10 } }
                    labelStyle={ { color: 'grey' } }
                    formatLabel={ value => value}
                />

              <LineChart
                  style={ { height: 200 } }
                  dataPoints={ data }
                  contentInset={ { top: 40, bottom: 10 } }
                  renderGradient={ ({ id }) => (
                      <LinearGradient id={ id } x1={ '0%' } y={ '0%' } x2={ '0%' } y2={ '100%' }>
                          <Stop offset={ '0%' } stopColor={ 'rgb(228, 35, 89)' } stopOpacity={ 0.8 }/>
                          <Stop offset={ '100%' } stopColor={ 'rgb(255, 156, 0)' } stopOpacity={ 0.9 }/>
                      </LinearGradient>
                  ) }
              />
              <XAxis
                    style={ { paddingVertical: 16 } }
                    values={ data }
                    formatLabel={ (value, index) => index*20 }
                    chartType={ XAxis.Type.BAR }
                    labelStyle={ { color: 'grey' } }
                />
              </View>
              }
              <View style={styles.cross_icon}>
                <TouchableHighlight onPress={() => this.nextpage() }>
                <Image Press={() => this.nextpage() } style={{  width: 45, height: 45 }} source={{uri:'cross'}} />
                </TouchableHighlight>
              </View>
              <View style={styles.trans_bg}>

              </View>
              <View style={styles.btm_tabNavigation}>
                  <View style = {{flex:1,flexDirection:'column',alignItems:'center',justifyContent:'center'}}><Image style={{ width: 32, height: 28, top:3, position:'absolute' }} source={{uri:'feedicon'}} /><Text style={{fontSize:10,color:'#fff', marginTop:12}}>FEED</Text></View>
                  <View style = {{flex:1,flexDirection:'column',alignItems:'center',justifyContent:'center'}}><Image style={{ width: 28, height: 28, top:3, position:'absolute'  }} source={{uri:'profileicon'}} /><Text style={{fontSize:10,color:'#fff', marginTop:12}}>PROFILE</Text></View>
                  <View style = {{flex:1,flexDirection:'column',alignItems:'center',justifyContent:'center'}}><Image style={{ width: 45, height: 45, marginTop:3 }} source={{uri:'bottom_tab_middlelogo'}} /><Text style={{fontSize:10,color:'#fff', marginTop:12}}> </Text></View>
                  <View style = {{flex:1,flexDirection:'column',alignItems:'center',justifyContent:'center'}}><Image style={{ width: 28, height: 31, top:3, position:'absolute'  }} source={{uri:'notificationicon'}} /><Text style={{fontSize:10,color:'#fff', marginTop:12}}>NOTIFICATION</Text></View>
                  <View style = {{flex:1,flexDirection:'column',alignItems:'center',justifyContent:'center'}}><Image style={{ width: 28, height: 28, top:3, position:'absolute'  }} source={{uri:'btnmenuicon'}} /><Text style={{fontSize:10,color:'#fff', marginTop:12}}>MORE</Text></View>


              </View>


            </View>



    );
  }
}

export default TrackActivitySummary;
