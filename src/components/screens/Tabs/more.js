import React, { Component } from 'react';
import { 
  View,
  TouchableOpacity,
  StyleSheet,
  Text
 } from 'react-native';
 import { onSignIn, isSignedIn, onSignOut, getAllAsyncStroage, removeBLE } from '../../../config/auth';

export default class More extends Component {

  constructor(props) {
      super(props);
      this.state={
        
      }
  }
  componentWillMount(){
    console.log(this.props)
    getAllAsyncStroage().then(res=>console.log(res)).catch(err=>console.log(err))
  }

  render() {
    return (
      <View style = {styles.container}>
         <TouchableOpacity onPress={()=>{
            const { navigate  } = this.props.navigation;
            onSignOut();
            navigate("SignedOut")
          }}>
            <Text style = {styles.text}>
               Logout
            </Text>
         </TouchableOpacity>
         <TouchableOpacity onPress={()=>{
            const { navigate  } = this.props.navigation;
            navigate('SearchDevices', {fromRoute:'More', toRoute:'SearchDevices', enableBack:false})
          }}>
            <Text style = {styles.text}>
               Add devices
            </Text>
         </TouchableOpacity>
         <TouchableOpacity onPress={()=>{
            const { navigate  } = this.props.navigation;
            removeBLE();
            navigate('Tabs', {fromRoute:'More', toRoute:'Tabs', enableBack:false})
          }}>
            <Text style = {styles.text}>
               Remove device
            </Text>
         </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create ({
   container: {
      marginTop:120,
      alignItems: 'center',
   },
   text: {
      borderWidth: 1,
      padding: 25,
      borderColor: 'black',
      backgroundColor: 'red'
   }
})