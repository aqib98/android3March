import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Switch,
  Image,
  PixelRatio,
  KeyboardAvoidingView,
  ScrollView,
  TouchableHighlight,
  TouchableOpacity,
  TextInput,
  AsyncStorage,
  Dimensions,
  Keyboard,
  ToastAndroid,
  Alert,
  ActivityIndicator
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/FontAwesome';
import { NavigationActions } from 'react-navigation';
import { onSignIn, getAllAsyncStroage } from '../../../config/auth';
import { getProfileDetails } from '../../template/SQLiteOperationsOffline.js';
import { updateProfile } from '../../template/api.js';
import API from '../../template/constants.js';
import axios from 'axios';
import FCM, {FCMEvent, RemoteNotificationResult, WillPresentNotificationResult, NotificationType} from 'react-native-fcm';
import styles from './styles.js';

import Toast, {DURATION} from 'react-native-easy-toast'
import StatusBarBackground from './statusbar.js'
const {height, width} = Dimensions.get('window');

const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

export default class UserInfo1 extends Component {

  constructor(props) {
    super(props);
    this.state = {
      firstname: null,
      lastname: null,
      username: null,
      password: "",
      loader:false,
      userid:"",
      contentLoader:true
    };
  }

  componentWillMount(){

    getAllAsyncStroage()
    .then(res =>
      console.log(res)
    )
    .catch(err =>
      console.log(err)
    );
    this.getStudentID();
  }

  getStudentID =async()=> {

    let USER_ID = await AsyncStorage.getItem("USER_ID");
    this.setState({userid:USER_ID,enable:false, Loader:true},()=>{
      this._getStudentDetails(this.state.userid);
    });
  }
  _getStudentDetails=(id)=>{
    getProfileDetails(id).then(response=>{
      if(response.status){
        this.setState({profile:response.data},()=>{
          this.setState({firstname: this.state.profile.first_name, lastname:this.state.profile.last_name, username:this.state.profile.user_name})
        });
        console.log(this.state)
        this.setState({Loader:false, contentLoader:false})
      }else{
        this.setState({Loader:false, contentLoader:false})
        //this.refs.toast.show(response.show, DURATION.LENGTH_LONG);
      }
    },err=>{
      console.log(err)
    })
  }


  nextstep = () => {
    //const { navigate } = this.props.navigation;

    const { firstname, lastname, username, userid } = this.state

    this.setState({contentLoader: true})

    if(firstname != null && lastname != null && username != null) {

      data = {
                "first_name": firstname,
                "last_name": lastname,
                "user_name": username,
                "id": userid
             }
        axios.put(API.UpdateProfile, data).then( async response => {
          if(response.status==200){
              console.log(response)
              AsyncStorage.setItem("USER_NAME", String(username));
              const { navigate } = this.props.navigation;
              this.setState({contentLoader: false})
              alert('Updated')
              navigate("UserInfo2", { password: null, confirmpassword: null, schoolid: null, schollname:null })
            }
          }).catch(err => {
            console.log(err)
          })

      } else {
        alert('Please Fill the fields')
        this.setState({contentLoader: false})
      }

    /*



    const { navigate } = this.props.navigation;
    navigate("UserInfo2", { password: null, confirmpassword: null, schoolid: null, schollname:null })

    //navigate("UserInfo2")

    */
  }

  render(){
    const { navigate } = this.props.navigation;
    const {
      loader,
      username,
      password,
      showErrorUsername,
      showErrorPassword,
      errorUsernameMessage,
      errorPasswordMessage,
      contentLoader
    } = this.state;
    return(
      <View style={ styles.mainBody }>
        <StatusBarBackground style={{backgroundColor:'#ff7200'}}/>
          <View style={styles.chevron_left_icon}>
            <TouchableOpacity onPress={()=>{
              const { navigate } = this.props.navigation;
              navigate('ProfileFill', { userinfo: 1, profiledetails: 0, pickclass: 0 })}}>
              <Icon name="chevron-left" size={25} color="#FF7E00"   />
            </TouchableOpacity>
          </View>
          <View style={styles.header}>
            <Text style={styles.topSignupTxt}>
              User Info (1/2)
            </Text>
          </View>

          {contentLoader?
            <ActivityIndicator
                animating = {this.state.contentLoader}
                color = '#bc2b78'
                size = "large"
                style = {styles.activityIndicator}
            />
          :
            <View>
            <View style={{marginTop: 50}}>
              <View style={styles.body1}>
                <View style={{marginTop: 25, marginBottom: 25}}>
                  <TextInput
                    placeholder="First name"
                    underlineColorAndroid='transparent'
                    autoCorrect={false}
                    placeholderTextColor='#626264'
                    style={styles.textInput_login}
                    value={this.state.firstname}
                    onChangeText={ (text) => this.setState({firstname:text}) }
                    onFocus={ () => this.setState({showErrorUsername:false,showErrorPassword:false}) }
                  />
                </View>

                <View style={{marginTop: 25, marginBottom: 25}}>
                  <TextInput
                    placeholder="Last name"
                    underlineColorAndroid='transparent'
                    autoCorrect={false}
                    placeholderTextColor='#626264'
                    style={styles.textInput_login}
                    value={this.state.lastname}
                    onChangeText={ (text) => this.setState({lastname:text}) }
                  />
                </View>

                <View style={{marginTop: 25, marginBottom: 25}}>
                  <TextInput
                    placeholder="User name"
                    underlineColorAndroid='transparent'
                    autoCorrect={false}
                    placeholderTextColor='#626264'
                    style={styles.textInput_login}
                    value={this.state.username}
                    onChangeText={ (text) => this.setState({username:text}) }
                    onFocus={ () => this.setState({showErrorUsername:false,showErrorPassword:false}) }
                  />
                </View>
              </View>
            </View>


            </View>



           }

            <View style={{bottom:20,position:'absolute',alignItems:'center',justifyContent:'center'}}>
                <TouchableOpacity onPress={()=>this.nextstep()}>
                    <View style={{zIndex:999,alignItems:'center',justifyContent:'center',height:50,width:width}}>


                        <Text style={{backgroundColor:'transparent',alignSelf:'center',fontFamily:'CircularStd-Black',color:'#fff',fontSize:19}}>Save and continue</Text>

                    </View>
                    <Image style={{width:width,height:50,position:'absolute',bottom:0}} source={{ uri: 'buttonimg' }}/>
                </TouchableOpacity>
            </View>


      </View>
    );
  }
}
