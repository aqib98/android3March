import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Switch,
  Image,
  PixelRatio,
  KeyboardAvoidingView,
  ScrollView,
  TouchableHighlight,
  TouchableOpacity,
  TextInput,
  AsyncStorage,
  Dimensions,
  Keyboard,
  ToastAndroid,
  Alert,
  ActivityIndicator, Picker, DatePickerIOS
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/FontAwesome';
import { NavigationActions } from 'react-navigation';
import { onSignIn,onSignOut, getAllAsyncStroage } from '../../../config/auth';
import { signin, getCourses } from '../../template/api.js'
import axios from 'axios';
import FCM, {FCMEvent, RemoteNotificationResult, WillPresentNotificationResult, NotificationType} from 'react-native-fcm';
import styles from './styles.js';
import moment from "moment";

import Toast, {DURATION} from 'react-native-easy-toast'
import StatusBarBackground from './statusbar.js'

const {height, width} = Dimensions.get('window');

const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');


export default class SelectWeight extends Component {

  constructor(props) {
    super(props);
    this.state = {
      weight: null,
      system: 0,
      weight_in_kgs: null
    };
  }

  componentWillMount(){

    var { feet, inches, weight, system, weight_in_kgs, height_in_metric  } = this.props.navigation.state.params;

    if(feet != null){
      this.setState({feet: feet})
    }

    if(inches != null){
      this.setState({inches: inches})
    }

    if(weight != null){
      this.setState({weight: weight})
    }

    if(system != null){
      this.setState({system: system})
    }

    if(height_in_metric != null){
      this.setState({height_in_metric: height_in_metric})
    }

     if(weight_in_kgs != null){
      this.setState({weight_in_kgs: weight_in_kgs})
    }

  }

  nextStep() {

    var { weight_in_kgs, weight } = this.state

    if(weight_in_kgs > 0 || weight > 0) {

      const { navigate } = this.props.navigation;


      console.log(weight_in_kgs)

      var { feet, inches, height_in_metric, system } = this.props.navigation.state.params;
      console.log(system)

      if(system == 1){
        //alert(weight_in_kgs)
        navigate("ProfileDetails2", {feet: feet, inches: inches, weight: null, height_in_metric: height_in_metric, system: system, weight_in_kgs: weight_in_kgs})
      } else {
        navigate("ProfileDetails2", {feet: feet, inches: inches, weight: weight, height_in_metric: height_in_metric, system: system, weight_in_kgs: null})
      }
    } else {
      alert('Please select valid weight')
    }
  }

  backStep = () => {

    const { navigate } = this.props.navigation;

    var { weight, feet, inches, system , height_in_metric, weight_in_kgs } = this.props.navigation.state.params;

    navigate("ProfileDetails2", {feet: feet, inches: inches, weight: weight, system: system, height_in_metric: height_in_metric, weight_in_kgs: weight_in_kgs})

  }

  render(){

  var payments1 = [];
  var payments2 = [];

    for(let i = 0; i < 300; i++){

      payments1.push(
          <Picker.Item label={i.toString()+' lbs'} value={i.toString()}  />
      )
      payments2.push(
          <Picker.Item label={i.toString()+' kg'} value={i.toString()}  />
      )
    }

    const { navigate } = this.props.navigation;
    return(
      <View style={ styles.mainBody }>
        <StatusBarBackground style={{backgroundColor:'#ff7200'}}/>
          <View style={styles.chevron_left_icon}>
            <TouchableOpacity onPress={()=>this.backStep()}>
              <Icon name="chevron-left" size={25} color="#FF7E00"   />
            </TouchableOpacity>
          </View>
          <View style={styles.header}>
            <Text style={styles.topSignupTxt}>
              Select Weight
            </Text>
          </View>

          {this.state.system == 0 ?
          <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between'}}>
            <View style={{width: width/2 }}>
              <Picker
                selectedValue={this.state.weight}
                onValueChange={(itemValue, itemIndex) => this.setState({weight: itemValue})}>
                {payments1}
              </Picker>
            </View>
            <View style={{width: width/2 }}>
              <Picker
                selectedValue={this.state.system}
                onValueChange={(itemValue, itemIndex) => this.setState({system: itemValue})}>
                <Picker.Item label="Imperial" value="1"  />
              </Picker>
            </View>
          </View>
          :
          <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between'}}>
            <View style={{width: width/2 }}>
              <Picker
                selectedValue={this.state.weight_in_kgs}
                onValueChange={(itemValue, itemIndex) => this.setState({weight_in_kgs: itemValue})}>
                {payments2}
              </Picker>
            </View>
            <View style={{width: width/2 }}>
              <Picker
                selectedValue={this.state.system}
                onValueChange={(itemValue, itemIndex) => this.setState({system: itemValue})}>
                <Picker.Item label="Metric" value="1"  />
              </Picker>
            </View>
          </View>
          }

          <View style={{bottom:20,position:'absolute',alignItems:'center',justifyContent:'center'}}>
              <TouchableOpacity onPress={()=>this.nextStep()}>
                  <View style={{zIndex:999,alignItems:'center',justifyContent:'center',height:50,width:width}}>


                      <Text style={{backgroundColor:'transparent',alignSelf:'center',fontFamily:'CircularStd-Black',color:'#fff',fontSize:19}}>Save</Text>

                  </View>
                  <Image style={{width:width,height:50,position:'absolute',bottom:0}} source={{ uri: 'buttonimg' }}/>
              </TouchableOpacity>
          </View>


      </View>
    );
  }
}
