import React, { Component } from 'react';
import { AppRegistry, StyleSheet, Text, View, TouchableHighlight, NativeAppEventEmitter, NativeEventEmitter, NativeModules, Platform, PermissionsAndroid, ListView, ScrollView, AppState, TouchableOpacity, Image, ToastAndroid, Alert, ActivityIndicator } from 'react-native';
import { AsyncStorage } from "react-native";
import Icon from 'react-native-vector-icons/FontAwesome';
import Dimensions from 'Dimensions';
import BleManager from 'react-native-ble-manager';
import TimerMixin from 'react-timer-mixin';
import reactMixin from 'react-mixin';
import moment from "moment";
import { getProfileDetails } from '../../template/SQLiteOperationsOffline.js';
import { addToTable, addSensorData } from '../../template/api.js';
const window = Dimensions.get('window');
const ds = new ListView.DataSource({
    rowHasChanged: (r1, r2) => r1 !== r2
});
import { Card, Button, FormLabel, FormInput, FormValidationMessage } from "react-native-elements";
import Video from 'react-native-video';
import { getExercises, getExerciseDetails, getSensorData, sendStatus } from '../../template/api.js';
import { addToSQLiteTable } from '../../template/SQLiteOperationsOffline.js';

import Toast, { DURATION } from 'react-native-easy-toast'
import KeepAwake from 'react-native-keep-awake';
import StatusBarBackground from './statusbar.js'

import styles from './styles.js'


const BleManagerModule = NativeModules.BleManager;
const bleManagerEmitter = new NativeEventEmitter(BleManagerModule);

export default class TrackExcierce extends Component {
    constructor() {
        super()

        this.state = {
            scanning: false,
            peripherals: new Map(),
            peripheralId: 0,
            appState: '',
            heartrate: 0,
            screenchange: '0',
            time: moment().format("LTS"),
            date: moment().format("LL"),
            totsec: 2400,
            item: 0,
            array: [],
            avg_heart_rate: [],
            sum: 0,
            average: 0,
            calories: 0,
            user_id: null,
            profile: [],
            height: '',
            weight: '',
            timepassed: false,
            rate: 1,
            volume: 1,
            duration: 0.0,
            currentTime: 0.0,
            paused: true,
            loader: false,
            times: 0,
            age: '',
            time: {},
            seconds: 0,
            device_id: '',
            increment: 0,
            startbutton: 0,
            bleInfo: {},
            connected: false,
            contentLoader: true,
            connected: false,
            bleDeviceConnected: false,
            exerciseStarted: false,
            images: ['back1', 'back2', 'back3', 'back4', 'main'],
            pauseReading: true,
            completeReading: false,
            startReading: true,
        }

        this.handleDiscoverPeripheral = this.handleDiscoverPeripheral.bind(this);
        this.handleStopScan = this.handleStopScan.bind(this);
        this.handleUpdateValueForCharacteristic = this.handleUpdateValueForCharacteristic.bind(this);
        this.handleDisconnectedPeripheral = this.handleDisconnectedPeripheral.bind(this);
        this.handleAppStateChange = this.handleAppStateChange.bind(this);
        this.timer = 0;
        this.startTimer = this.startTimer.bind(this);
        this.countDown = this.countDown.bind(this);

    }


    async componentWillMount() {

        console.log('componentWillMount')

        let user_id = await AsyncStorage.getItem("USER_ID");
        let device_id = await AsyncStorage.getItem("DEVICE_ID");



        let device_name = await AsyncStorage.getItem("DEVICE_NAME");
        const {navigate} = this.props.navigation;
        console.log(device_id, device_name)
        if (!device_id || !device_name) {
            navigate('SearchDevices')
        }

        this._getStudentDetails(user_id);

        this.bleConnect(this.state.device_id)

        this.setTimeout(() => {

            console.log('asdasdasd')

            this.checkstate(this.state.connected)

        }, 10000);



        KeepAwake.activate();

    }


    checkstate(connected) {
        const {navigate} = this.props.navigation;
        if (this.state.connected === false) {
            Alert.alert('Make Sure Your Device is Near By', 'Please Re Connect',
                [
                    {
                        text: 'Ok',
                        onPress: () => navigate('UpcomingWorkouts')
                    },
                ],
                {
                    cancelable: false
                }
            )
        } else {
            this.state.contentLoader = false

            this.startTimer()

            let timeLeftVar = this.secondsToTime(this.state.seconds1);

            this.setState({
                time: timeLeftVar
            });

        }

    }

    async componentDidMount() {
        console.log('componentDidMount')
        this.setState({
            device_id: await AsyncStorage.getItem("DEVICE_ID")
        }, () => {

        })


        //this.refs.toast.show('Connecting to saved device....' , DURATION.LENGTH_LONG);
        AppState.addEventListener('change', this.handleAppStateChange);



        this.handlerDiscover = bleManagerEmitter.addListener('BleManagerDiscoverPeripheral', this.handleDiscoverPeripheral);
        this.handlerStop = bleManagerEmitter.addListener('BleManagerStopScan', this.handleStopScan);
        this.handlerDisconnect = bleManagerEmitter.addListener('BleManagerDisconnectPeripheral', this.handleDisconnectedPeripheral);
        this.handlerUpdate = bleManagerEmitter.addListener('BleManagerDidUpdateValueForCharacteristic', this.handleUpdateValueForCharacteristic);

    }


    _getStudentDetails=(id) => {
        getProfileDetails(id).then(response => {
            console.log(response)
            if (response.status) {
                this.setState({
                    profile: response.data
                }, () => {
                    console.log(this.state.profile)
                    this.setState({
                        height: this.state.profile.height_in_feet,
                        age: this.state.profile.age,
                        weight: this.state.profile.current_weight,
                        gender: this.state.profile.gender
                    }, () => {

                        if (this.state.profile.parameter_type == 1) {

                            this.setState({
                                height: this.state.profile.height_in_centimeter
                            })

                        } else {

                            var height_incm = (this.state.feet * 30.48) + (this.state.inches * 2.54)

                            this.setState({
                                height: height_incm
                            })
                        }

                    });
                });
                console.log(this.state)
            //this.setState({Loader:false, contentLoader:false})
            } else {
                //this.setState({Loader:false, contentLoader:false})
                this.refs.toast.show(response.show, DURATION.LENGTH_LONG);
            }
        }, err => {
            console.log(err)
        })
    }


    addBLERoute=async () => {
        const {navigate} = this.props.navigation;
        Alert.alert(
            'Maku sure your device is near By', 'Start actvity again',
            [
                {
                    text: 'Ok',
                    onPress: () => navigate('UpcomingWorkouts')
                },
            ],
            {
                cancelable: false
            }
        )
    }


    bleConnect(device_id) {
        let x = 0

        console.log(device_id)

        BleManager.connect(device_id).then(() => {
            console.log('inside connect')

            this.setState({
                connected: true
            })

            this.setTimeout(() => {

                BleManager.retrieveServices(device_id).then((peripheralInfo) => {

                    var service = '180D';
                    var bakeCharacteristic = '2A37';
                    var crustCharacteristic = '13333333-3333-3333-3333-333333330001';

                    this.setTimeout(() => {
                        BleManager.startNotification(device_id, service, bakeCharacteristic).then(() => {


                            console.log('Started notification on ' + device_id);
                            this.state.startbutton = 1;


                        }).catch((error) => {

                            this.refs.toast.show('Make Sure your Device is near by.', DURATION.LENGTH_LONG);

                            this.state.loader = false

                            this.state.startbutton = 0

                            console.log('Connection error', error);


                        });
                    }, 200);
                }).catch((error) => {

                    this.refs.toast.show('Make Sure your Device is near by.', DURATION.LENGTH_LONG);

                    this.state.loader = false

                    this.state.startbutton = 0

                    console.log('Connection error', error);


                });

            }, 900);

        }).catch((error) => {

            this.refs.toast.show('Make Sure your Device is near by.', DURATION.LENGTH_LONG);

            this.state.loader = false

            this.state.startbutton = 0

            console.log('Connection error', error);

        });

    }


    handleAppStateChange(nextAppState) {
        if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
            console.log('App has come to the foreground!')
            BleManager.getConnectedPeripherals([]).then((peripheralsArray) => {
                console.log('Connected peripherals: ' + peripheralsArray.length);
            });
        }
        this.setState({
            appState: nextAppState
        });
    }



    handleDisconnectedPeripheral(data) {
        let peripherals = this.state.peripherals;
        let peripheral = peripherals.get(data.peripheral);
        if (peripheral) {
            peripheral.connected = false;
            peripherals.set(peripheral.id, peripheral);
            this.setState({
                peripherals
            });

        }
        console.log('Disconnected from ' + data.peripheral);

    }


    handleUpdateValueForCharacteristic(data) {
        //console.log('Received data from ' + data.peripheral + ' characteristic ' + data.characteristic, data.value);
        this.setState({
            heartrate: data.value[1]
        });

        if (!this.state.pauseReading) {
            this.savedata(data.value[1])
        }
    }

    savedata(data) {

        this.state.array[this.state.item] = data;

        if (this.state.item == 20) {
            this.setState(prevState => ({
                times: prevState.times + 20
            }))
            for (var i = 0; i < this.state.array.length; i++) {
                this.state.sum += parseInt(this.state.array[i]);
            }

            if (this.state.array.length > 0) {

                this.state.average = Math.round(this.state.sum / this.state.array.length);

                this.state.avg_heart_rate[this.state.increment] = this.state.average;

                if (this.state.age == null) {
                    this.state.age = 25
                }

                //console.log(this.state.age)

                let calories = Math.round((0.4472 * this.state.average - 0.05741 * this.state.weight + 0.074 * this.state.age - 20.4022) * 0.333 / 4.184)

                //let calories = Math.round((-55.0969 + (0.6309 * this.state.average) + (0.1988 * (this.state.weight/2.2046) + (0.2017 * this.state.age))/4.184) * 60 * 0.005)

                if (calories < 0 || calories == 'null') {
                    calories = 0
                }

                this.state.calories = calories + this.state.calories;

                this.state.increment++

                console.log(this.state.age)

                console.log(this.state.calories)

            }

            this.state.item = 0;

            this.state.array = [];

            this.state.sum = 0;

        } else {

            this.state.item = this.state.item + 1;

        }

    }


    handleStopScan() {
        console.log('Scan is stopped');
        this.setState({
            scanning: false
        });
    }

    startScan() {
        if (!this.state.scanning) {
            BleManager.scan([], 30, true).then((results) => {
                console.log('Scanning...');
                this.refs.toast.show('Scanning....', DURATION.LENGTH_LONG);
                this.setState({
                    scanning: true
                });
            }).catch((error) => {
                console.log('Connection error', error);
                this.refs.toast.show('Connection error.... Please Try To Reconnect', DURATION.LENGTH_LONG);
            });
        }
    }

    handleDiscoverPeripheral(peripheral) {
        var peripherals = this.state.peripherals;
        if (!peripherals.has(peripheral.id)) {
            console.log('Got ble peripheral', peripheral);
            peripherals.set(peripheral.id, peripheral);
            this.setState({
                peripherals
            })
        }
    }


    /*** Timer Functionality ***/

    startTimer() {
        console.log('Starting')
        if (this.timer == 0) {
            this.timer = setInterval(this.countDown, 1000);
        }
    }

    countDown() {
        // Remove one second, set state so a re-render happens.

        if (!this.state.pauseReading) {
            var seconds = this.state.seconds + 1;
        } else {
            var seconds = this.state.seconds
        }
        this.setState({
            time: this.secondsToTime(seconds),
            seconds: seconds,
        });

        // Check if we're at zero.
        if (seconds == 0) {
            //clearInterval(this.timer);
        }
    }

    secondsToTime(secs) {

        let hours = Math.floor(secs / (60 * 60));

        let divisor_for_minutes = secs % (60 * 60);
        let minutes = Math.floor(divisor_for_minutes / 60);

        let divisor_for_seconds = divisor_for_minutes % 60;
        let seconds = Math.ceil(divisor_for_seconds);

        if (minutes < 10) {
            minutes = '0' + minutes
        }

        if (hours < 10) {
            hours = '0' + hours
        }

        if (seconds < 10) {
            seconds = '0' + seconds
        }

        let obj = {
            "h": hours,
            "m": minutes,
            "s": seconds
        };
        return obj;
    }

    _sendStatus(deviceid, avg_heart_rate, cal, hou, min, sec) {

        BleManager.disconnect(deviceid)
            .then(() => {
                // console.log('Disconnected');
            })
            .catch((error) => {
                //console.log(error);
            });

        let sum = 0

        //console.log(avg_heart_rate)
        for (var i = 0; i < avg_heart_rate.length; i++) {
            sum += parseInt(avg_heart_rate[i])
        }

        var time = hou + ':' + min + ':' + sec

        sum = Math.round(sum / avg_heart_rate.length);

        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();

        if (dd < 10) {
            dd = '0' + dd
        }

        if (mm < 10) {
            mm = '0' + mm
        }

        date = yyyy + '-' + mm + '-' + dd;

        const {navigate} = this.props.navigation;
        navigate("Logactivity", {
            calories: cal,
            time: time,
            date: date,
            heart_rate: sum,
            avg_heart_rate: avg_heart_rate,
            fromScreen: 'Trackactivity'
        });

        //navigate("Tabs")


    }

    completedReading = () => {


        if (!this.state.completeReading) {
            this.setState({
                completeReading: true,
                pauseReading: true
            })
        } else {
            this.setState({
                completeReading: false
            })
        }



    }


    componentWillUnmount() {

        BleManager.disconnect(this.state.device_id)
            .then(() => {
                //console.log('Disconnected');
            })
            .catch((error) => {
                //console.log(error);
            });
        this.setState({});
        this.handlerDiscover.remove();
        this.handlerStop.remove();
        this.handlerDisconnect.remove();
        this.handlerUpdate.remove();


    }

    render() {


        const list = Array.from(this.state.peripherals.values());

        for (i = 0; i < list.length; i++) {
            if (list[i].id == this.state.device_id) {
                var device_data = list[i]
            }
        }

        var {bleDeviceConnected, exerciseStarted, heartrate, images, contentLoader, completeReading} = this.state;
        let imageURI = 'gradientbg'

        if (heartrate < 90 && heartrate > 40) {
            imageURI = images[0]
        } else if (heartrate > 90 && heartrate < 150) {
            imageURI = images[3]
        } else if (heartrate > 150 && heartrate < 170) {
            imageURI = images[1]
        } else if (heartrate > 170 && heartrate < 190) {
            imageURI = images[2]
        }

        return (
            <View>
          {contentLoader ?
                <View style={styles.trackactivity_container}>
              <View style={styles.trackactivity_image_container}>
                <Image style={styles.trackactivity_bg_image} source={{
                    uri: 'back1'
                }}/>
              </View>
              <View style={styles.connectingscreen}>

                <Text style={styles.trackactivity_subSection_view_part1_headingtext1}> Connecting to Heartrate Monitor</Text>

                  <ActivityIndicator
                animating = "true"
                color = 'blue'
                size = "large"
                style = {styles.activityIndicator}
                />
              </View>
            </View>
                :
                <View>
                  {completeReading ?
                    <View style={styles.trackactivity_container}>
              <View style={styles.trackactivity_image_container}>
                <Image style={styles.trackactivity_bg_image} source={{
                        uri: 'back3'
                    }}/>
              </View>
                    <View style={styles.stopactivity}>

                    <Text style={styles.trackactivity_subSection_view_part1_stop}>STOP TRACKING ACTIVITY</Text>

                    <View style={styles.trackactivity_start_stop_btn_view}>
                      <TouchableOpacity
                    activeOpacity={1}
                    style={styles.trackactivity_start_stop_btn_touchable_opacity}
                    onPress={() => this._sendStatus(this.state.device_id, this.state.avg_heart_rate, this.state.calories, this.state.time.h, this.state.time.m, this.state.time.s)}>
                        <Text style={styles.trackactivity_start_stop_btn_text}>COMPLETE</Text>
                      </TouchableOpacity>
                    </View>

                    </View>

                    </View>

                    :
                    <View style={styles.trackactivity_container}>
                    <View style={styles.trackactivity_image_container}>
                      <Image style={styles.trackactivity_bg_image} source={{
                        uri: imageURI
                    }}/>
                    </View>
                    <StatusBarBackground style={{
                        backgroundColor: '#ff7200'
                    }}/>
                      <View style={styles.chevron_left_icon}>
                        <TouchableOpacity onPress={() => {
                        const {navigate} = this.props.navigation;
                        navigate('UpcomingWorkouts')
                    }}>
                          <Icon name="chevron-left" size={25} color="#FF7E00"   />
                        </TouchableOpacity>
                      </View>
                      <View style={styles.header}>
                        <Text style={styles.topSignupTxt}>
                          TRACK ACTIVITY!
                        </Text>
                      </View>
                    <View>
           <View style={styles.trackactivity_heartimg_heartrate_view}>
              {heartrate.toString().length < 3 ?
                        <View><Icon name="heart-o" size={120} style={styles.trackactivity_heart_img}   /><Text style={styles.trackactivity_heartrate}>{this.state.heartrate}</Text></View>
                        :
                        <View><Icon name="heart-o" size={100} style={styles.trackactivity_heart_img}   /><Text style={styles.trackactivity_heartrate1}>{this.state.heartrate}</Text></View>
                    }
           </View>

           <View style={styles.trackactivity_subSection_view1}>
             <View style={styles.trackactivity_subSection_view_part1}>
               <Text style={styles.trackactivity_subSection_view_part1_calories}>{this.state.calories}</Text>
               <Text style={styles.trackactivity_subSection_view_part1_headingtext}>CALORIES</Text>
             </View>

             <View style={styles.trackactivity_subSection_view_part1}>
               <Text style={styles.trackactivity_subSection_view_part1_calories}>{this.state.average}</Text>
               <Text style={styles.trackactivity_subSection_view_part1_headingtext}>AVG BPM</Text>
             </View>

           </View>
           <View style={styles.trackactivity_subSection_view2}>
             <View style={styles.trackactivity_subSection_view_part1}>
               <Text style={styles.trackactivity_subSection_view_part1_calories}></Text>
               <Text style={styles.trackactivity_subSection_view_part1_headingtext}></Text>
             </View>

             <View style={styles.trackactivity_subSection_view_part1}>
              {this.state.startReading ?
                        <Text style={styles.trackactivity_subSection_view_part1_calories}>  00:00:00 </Text>
                        :
                        <Text style={styles.trackactivity_subSection_view_part1_calories}>  {this.state.time.h}:{this.state.time.m}:{this.state.time.s} </Text>
                    }
               <Text style={styles.trackactivity_subSection_view_part1_headingtext}>TIME</Text>
             </View>
           </View>

           {this.state.pauseReading ?

                        <View style={styles.trackactivity_start_stop_btn_view}>
             <TouchableOpacity
                        activeOpacity={1}
                        style={styles.trackactivity_start_stop_btn_touchable_opacity}
                        onPress={() => this.setState({
                            pauseReading: false,
                            startReading: false
                        })}>
               <Text style={styles.trackactivity_start_stop_btn_text}>{this.state.startReading ? 'START' : 'RESUME'}</Text>
                 </TouchableOpacity>
           </View>

                        :

                        <View style={styles.trackactivity_start_stop_btn_view}>
             <TouchableOpacity
                        activeOpacity={1}
                        style={styles.trackactivity_start_stop_btn_touchable_opacity}
                        onPress={() => this.setState({
                            pauseReading: true
                        })}>
               <Text style={styles.trackactivity_start_stop_btn_text}>PAUSE</Text>
                 </TouchableOpacity>
           </View>

                    }
          </View>
                      </View>


                }

        <View style={styles.cross_icon1}>
          <TouchableOpacity
                activeOpacity={1} onPress={() => this.completedReading()
                }>
            <Image style={{
                    width: 45,
                    height: 45
                }} source={{
                    uri: 'cross'
                }} />
          </TouchableOpacity>
        </View>
        <View style={styles.trans_bg}>
          <Image style={{
                    width: '100%',
                    height: 84
                }} source={{
                    uri: 'trans_bg'
                }} />
        </View>


                </View>
            }
          </View>
        );
    }
}
reactMixin(TrackExcierce.prototype, TimerMixin);
