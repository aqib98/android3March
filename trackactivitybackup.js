import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
  NativeAppEventEmitter,
  NativeEventEmitter,
  NativeModules,
  Platform,
  PermissionsAndroid,
  ListView,
  ScrollView,
  AppState,
  TouchableOpacity, Image, ToastAndroid
} from 'react-native';
import { AsyncStorage } from "react-native";
import Dimensions from 'Dimensions';
import BleManager from 'react-native-ble-manager';
import TimerMixin from 'react-timer-mixin';
import reactMixin from 'react-mixin';
import moment from "moment";
import { getProfileDetails } from '../../template/SQLiteOperationsOffline.js';
import { addToTable, addSensorData } from '../../template/api.js';
const window = Dimensions.get('window');
const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
import { Card, Button, FormLabel, FormInput, FormValidationMessage } from "react-native-elements";
import Video from 'react-native-video';
import { getExercises, getExerciseDetails, getSensorData, sendStatus } from '../../template/api.js';
import { addToSQLiteTable } from '../../template/SQLiteOperationsOffline.js';
const BleMan = NativeModules.BleMan;
import KeepAwake from 'react-native-keep-awake';

const BleManagerModule = NativeModules.BleManager;
const bleManagerEmitter = new NativeEventEmitter(BleManagerModule);

export default class TrackExcierce extends Component {
  constructor(){
    super()

    this.state = {
      scanning:false,
      peripherals: new Map(),
      appState: '',
      heartrate: 0,
      screenchange: '0',
      time: moment().format("LTS"),
      date: moment().format("LL"),
      totsec: 2400,
      item: 0,
      array: [],
      avg_heart_rate: [],
      sum: 0,
      average: 0,
      calories: 0,
      user_id:null,
      profile:[],
      height:'',
      weight:'',
      timepassed: false,
      rate: 1,
      volume: 1,
      duration: 0.0,
      currentTime: 0.0,
      paused: true,
      loader:false,
      times:0,
      age:'',
      time: {}, seconds: 0,device_id:'',increment:0,startbutton:0,m:0,h:0,s:0,
      bleDeviceConnected:false,
      exerciseStarted:false
    }

    this.handleDiscoverPeripheral = this.handleDiscoverPeripheral.bind(this);
    this.handleStopScan = this.handleStopScan.bind(this);
    this.handleUpdateValueForCharacteristic = this.handleUpdateValueForCharacteristic.bind(this);
    this.handleDisconnectedPeripheral = this.handleDisconnectedPeripheral.bind(this);
    this.handleAppStateChange = this.handleAppStateChange.bind(this);
    this.timer = 0;
    this.startTimer = this.startTimer.bind(this);
    this.countDown = this.countDown.bind(this);

  }


 async componentWillMount() {

  let user_id = await AsyncStorage.getItem("USER_ID");
  this.setState({user_id: user_id})
  let device_id = await AsyncStorage.getItem("DEVICE_ID");
  let device_name = await AsyncStorage.getItem("DEVICE_NAME");
  const { navigate  } = this.props.navigation;
  console.log(device_id, device_name)
    if(!device_id||!device_name){
      navigate('SearchDevices')
    }

  this._getStudentDetails(user_id);
  KeepAwake.activate();

  }

  async componentDidMount() {
    this.setState({device_id: await AsyncStorage.getItem("DEVICE_ID")},()=>{
      //this.bleConnect();
    })

  }


  _getStudentDetails=(id)=>{
    getProfileDetails(id).then(response=>{
      console.log(response)
      if(response.status){
        this.setState({profile:response.data},()=>{
          this.setState({height:this.state.profile.height_in_feet, age:this.state.profile.age, weight:this.state.profile.current_weight,gender:this.state.profile.gender},()=>{

          });
        });
        console.log(this.state)
        this.setState({Loader:false, contentLoader:false})
      }else{
        this.setState({Loader:false, contentLoader:false})
        ToastAndroid.show(response.show, ToastAndroid.SHORT);
      }
    },err=>{
      console.log(err)
    })
  }

  callback = (result) => {
      console.log(result)
      this.setState({number:result})
    }

    handleBleButtonClick(value){
      console.log('hi')
      BleMan.scanDevice('aqib',this.callback)
    }

  bleConnect=()=>{
    ToastAndroid.show('Connecting to saved device....' , ToastAndroid.SHORT);
    AppState.addEventListener('change', this.handleAppStateChange);

    BleManager.start({showAlert: false});

    this.handlerDiscover = bleManagerEmitter.addListener('BleManagerDiscoverPeripheral', this.handleDiscoverPeripheral );
    this.handlerStop = bleManagerEmitter.addListener('BleManagerStopScan', this.handleStopScan );
    this.handlerDisconnect = bleManagerEmitter.addListener('BleManagerDisconnectPeripheral', this.handleDisconnectedPeripheral );
    this.handlerUpdate = bleManagerEmitter.addListener('BleManagerDidUpdateValueForCharacteristic', this.handleUpdateValueForCharacteristic );


    if (Platform.OS === 'android' && Platform.Version >= 23) {
        PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION).then((result) => {
            if (result) {
              console.log("Permission is OK");
            } else {
              PermissionsAndroid.requestPermission(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION).then((result) => {
                if (result) {
                  console.log("User accept");
                } else {
                  console.log("User refuse");
                }
              });
            }
      });
    }


    this.state.loader = true;

     console.log(this.state.device_id)


        BleManager.connect(this.state.device_id).then(() => {

          this.setTimeout(() => {

            BleManager.retrieveServices(this.state.device_id).then((peripheralInfo) => {
              console.log(peripheralInfo);
              var service = '0000180d-0000-1000-8000-00805f9b34fb';
              var bakeCharacteristic = '00002a37-0000-1000-8000-00805f9b34fb';

              this.state.startbutton = 1;

              this.setTimeout(() => {
                BleManager.startNotification(this.state.device_id, service, bakeCharacteristic).then(() => {

                  this.state.loader = true;
                  ToastAndroid.show('Connected..' , ToastAndroid.SHORT);
                  console.log('Started notification on ' + this.state.device_id);

                  this.startTimer()

                  let timeLeftVar = this.secondsToTime(this.state.seconds);

                  this.setState({ time: timeLeftVar });
                  this.setState({bleDeviceConnected:true});

                  this.setState({exerciseStarted:true})

                }).catch((error) => {

                  this.state.loader = false;
                  ToastAndroid.show('Failed to connect, Check your device is turned on.' , ToastAndroid.LONG);
                  console.log('Notification error', error);
                });
              }, 1000);
            });

          }, 1000);


        }).catch((error) => {
          console.log('Connection error', error);

          this.state.loader = false;

          this.state.startbutton = 0;

          ToastAndroid.show('Make sure your heart rate sensor is nearby' , ToastAndroid.LONG);
        });

  }
  handleAppStateChange(nextAppState) {
    if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
      console.log('App has come to the foreground!')
      BleManager.getConnectedPeripherals([]).then((peripheralsArray) => {
        console.log('Connected peripherals: ' + peripheralsArray.length);
      });
    }
    this.setState({appState: nextAppState});
  }



  handleDisconnectedPeripheral(data) {
    let peripherals = this.state.peripherals;
    let peripheral = peripherals.get(data.peripheral);
    if (peripheral) {
      peripheral.connected = false;
      peripherals.set(peripheral.id, peripheral);
      this.setState({peripherals});
    }
    console.log('Disconnected from ' + data.peripheral);
  }

  handleUpdateValueForCharacteristic(data) {
    console.log('Received data from ' + data.peripheral + ' characteristic ' + data.characteristic, data.value);
      this.setState({heartrate: data.value[1]});
      this.savedata(data.value[1])

  }

  savedata(data) {

    this.state.array[this.state.item] = data;

    if(this.state.item == 20)
    {
    this.setState(prevState=>({times:prevState.times+20}))
    for (var i = 0; i < this.state.array.length; i++) {
        this.state.sum += parseInt(this.state.array[i]);
    }

    if (this.state.array.length > 0) {

        this.state.average = Math.round(this.state.sum/this.state.array.length);

        this.state.avg_heart_rate[this.state.increment] = this.state.average;

        if(this.state.age == null) { this.state.age = 25 }

        let calories = Math.round((0.4472 * this.state.average - 0.05741 * this.state.weight + 0.074 * this.state.age - 20.4022) * 0.333 / 4.184)

        //let calories = Math.round((-55.0969 + (0.6309 * this.state.average) + (0.1988 * (this.state.weight/2.2046) + (0.2017 * this.state.age))/4.184) * 60 * 0.005)

        if(calories < 0)
        {
            calories=0
        }

        this.state.calories = calories + this.state.calories;

        this.state.increment++

        console.log(this.state.age)

        console.log(this.state.calories)


          this._storeInDB();


    }

    this.state.item = 0;

    this.state.array = [];

    this.state.sum = 0;

    }
    else {

    this.state.item = this.state.item + 1;

    }

  }


  handleStopScan() {
    console.log('Scan is stopped');
    this.setState({ scanning: false });
  }

  startScan() {
    if (!this.state.scanning) {
      BleManager.scan([], 30, true).then((results) => {
        console.log('Scanning...');
        ToastAndroid.show('Scanning....' , ToastAndroid.SHORT);
        this.setState({scanning:true});
      }).catch((error) => {
          console.log('Connection error', error);
          ToastAndroid.show('Connection error.... Please Try To Reconnect' , ToastAndroid.SHORT);
      });
    }
  }

  handleDiscoverPeripheral(peripheral){
    var peripherals = this.state.peripherals;
    if (!peripherals.has(peripheral.id)){
      console.log('Got ble peripheral', peripheral);


      peripherals.set(peripheral.id, peripheral);
      this.setState({ peripherals })
    }
  }


  /*** Timer Functionality ***/

  startTimer() {
    console.log('Starting')
    if (this.timer == 0) {
      this.timer = setInterval(this.countDown, 1000);
    }
  }

  countDown() {
    // Remove one second, set state so a re-render happens.
    let seconds = this.state.seconds + 1;
    this.setState({
      time: this.secondsToTime(seconds),
      seconds: seconds,
    });

    // Check if we're at zero.
    if (seconds == 0) {
      //clearInterval(this.timer);
    }
  }

  secondsToTime(secs){

    let hours = Math.floor(secs / (60 * 60));

    let divisor_for_minutes = secs % (60 * 60);
    let minutes = Math.floor(divisor_for_minutes / 60);

    let divisor_for_seconds = divisor_for_minutes % 60;
    let seconds = Math.ceil(divisor_for_seconds);

    if(minutes < 10)
    {
      minutes = '0'+minutes
    }

    if(hours < 10)
    {
      hours = '0'+hours
    }

    if(seconds < 10)
    {
      seconds = '0'+seconds
    }

    let obj = {
      "h": hours,
      "m": minutes,
      "s": seconds
    };
    return obj;
  }

  _sendStatus(avg_heart_rate,cal, hou, min, sec){

        BleManager.disconnect(this.state.device_id)
          .then(() => {
              console.log('Disconnected');
          })
          .catch((error) => {
              console.log(error);
        });

        let sum = 0

        console.log(avg_heart_rate)
        for (var i = 0; i < avg_heart_rate.length; i++) {
            sum += parseInt(avg_heart_rate[i])
        }

        var time = hou+':'+min+':'+sec

        sum = Math.round(sum/avg_heart_rate.length);

        date = new Date().toISOString();

        const { navigate  } = this.props.navigation;
        navigate("Logactivity", { calories:cal, time: time, date: date, heart_rate: sum, avg_heart_rate: avg_heart_rate, fromScreen: 'Trackactivity'});

  }


  _storeInDB(){

  var exerciseID = Math.floor(1000 + Math.random() * 9000);

  var workoutID = Math.floor(1000 + Math.random() * 9000);

  var workout_day = 0;

  var courseID = 0;

  var { average, calories, times, user_id } = this.state;
  var data={
    "user_id":user_id,
    "avg_heart_rate" : average,
    "calories" : calories,
    "exercise_id": exerciseID,
    "course_id": courseID,
    "workout_id": workoutID,
    "workout_day": workout_day
  }
  var toSQL=[user_id, exerciseID, average, times];
  addToSQLiteTable({table_name:'sensor_data', table_data:toSQL}).then(response=>{
      console.log(response);
    },error=>{
      console.log(error);
    })
  }

  render() {
    var { bleDeviceConnected, exerciseStarted } = this.state;
    return(

          <View style={styles.container}>

                <ScrollView style={styles.scroll}>

                <View style={styles.heartrate1}>
                  <Text style={styles.heartrate1text}>{this.state.heartrate}</Text>
                  <Text>Heart Rate</Text>
                </View>

                <View style={styles.avgrateblock}>

                  <View style={styles.heartrate2}>
                  <Text style={styles.heartrate2text}>{this.state.average}</Text>
                  <Text style={{textAlign: 'center'}}>Avg. Heart Rate</Text>
                  </View>
                  <View style={styles.heartrate3}>
                  <Text style={styles.heartrate2text}>{this.state.calories}</Text>
                  <Text style={{textAlign: 'center'}}>Calories Burned</Text>
                  </View>

                </View>

                <View style={styles.heartrate1}>
                  <Text style={styles.heartrate1text}>{this.state.time.h}:{this.state.time.m}:{this.state.time.s}</Text>
                  <Text style={{textAlign: 'center'}}>Time</Text>
                </View>

                </ScrollView>
            {this.state.startbutton == 0?
            <View>
               <TouchableHighlight style={styles.loginButton1} onPress={() => this.bleConnect() }>
                  <Text style={{textAlign:'center'}}>Start Workout</Text>
               </TouchableHighlight>
            </View>:
            <View style={styles.loginButton}>
               <TouchableHighlight onPress={() => this._sendStatus(this.state.avg_heart_rate, this.state.calories, this.state.time.h, this.state.time.m, this.state.time.s) }>
                  <Text style={{textAlign:'center'}}>Complete Workout</Text>
               </TouchableHighlight>
            </View>}

           <TouchableOpacity style={{marginTop: 0,margin: 20, padding:20, backgroundColor:'#ccc'}} onPress={this.handleBleButtonClick}>
                     <Text>Connect Watch</Text>
                   </TouchableOpacity>
          </View>

    );
  }
}
reactMixin(TrackExcierce.prototype, TimerMixin);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
  save_btnTxt : {
    fontSize:20,
    paddingTop:35,
    fontWeight:'bold',
    textAlign:'center',
    color:'#fff',
    justifyContent: 'center',
    alignItems: 'center'},

    save_btnImg:{
        resizeMode: 'contain',

        height:100,
         width:Dimensions.get('window').width,
         zIndex:0
    },

    save_view : {

      position: 'absolute',
      zIndex:999,
      width:Dimensions.get('window').width,
      justifyContent: 'center',
      backgroundColor: 'transparent'
    },

    signup_btnTxt : {
      fontSize:20,
      paddingTop:35,
      fontWeight:'bold',
      textAlign:'center',
      color:'#fff',
      justifyContent: 'center',
      alignItems: 'center'},

      signup_btnImg:{

          width:220,
          height:100,

           zIndex:999
      },
  hearbeat: {
    fontSize: 24,
    alignSelf: 'center'
  },
  heartrate1: {
    alignSelf: 'center',
    marginTop: 20,
    borderBottomWidth: 2
  },
  heartrate1text: {
    textAlign: 'center',
    fontSize: 50,
    color: '#ef7e2d'
  },
  avgrateblock: {
    flex: 1,
    width: '100%',
    flexDirection: 'row',
    alignSelf: 'center'
  },
  heartrate2: {
    width: '50%',
    alignSelf: 'center',
    justifyContent: 'center'
  },
  heartrate3: {
    width: '50%',
    alignSelf: 'flex-end',
  },
  heartrate2text: {
    textAlign: 'center',
    fontSize: 50,
    color: '#ef7e2d',
    alignSelf: 'center'
  },
  heartrate3text: {
    textAlign: 'center',
    fontSize: 50,
    color: '#ef7e2d',
    alignSelf: 'center'
  },
  loginButton: {
     bottom: 0,
     height:60,
     backgroundColor:'orangered',
     justifyContent:'center',
     width: '100%',
   },
   loginButton1: {
      bottom: 0,
      height:60,
      backgroundColor:'orangered',
      justifyContent:'center',
      width: '100%',
    },
});
