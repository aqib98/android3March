React Native Application

For ANDROID:

To create APK file:-
	1:- Open command prompt, Go to android and run command gradlew assembleRelease

	gradlew assembleRelease --console plain


	1. Install the "SDK DEMO" Android app
	2. Wear the watch and click the down button
	3. Follow the instructions as like in video
	4. Disconnect the Device
	5. Install the "Health App"
	6. Login with existing details
	7. Add BLE Device
	8. Go to upcoming workouts page.
	9. Go to Track Activity Page.
	10. Click on Connect Watch button
	11. Reconnect the Device.
	12. Click on Real time Data.
	14. Change the watch mode to heart rate
  15. Now you can see the heart rate in the array
